module.exports = {
  siteMetadata: {
    title: "Business Professional",
    author: "Chris Rotunno",
    description: `Description placeholder`,
    social: [
      {
        name: `twitter`,
        url: `https://twitter.com/ChrisRotunno`
      },
      {
        name: `linkedin`,
        url: `https://www.linkedin.com/in/christopherrotunno/`
      }
    ]
  },
  plugins: [
    "gatsby-plugin-react-helmet",
    {
      resolve: "gatsby-source-contentful",
      options: {
        spaceId: "2dr8axgj9lo9",
        accessToken: "A-gUA7XOeaSUAMaNaj9lmCFQ6WJOyFPnyKXR7M5pgIk"
      }
    },
    "gatsby-plugin-sass",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "src",
        path: `${__dirname}/src/`
      }
    },
    "gatsby-plugin-sharp",
    {
      resolve: "gatsby-transformer-remark",
      options: {
        plugins: [
          "gatsby-remark-relative-images",
          {
            resolve: "gatsby-remark-images",
            options: {
              maxWidth: 750,
              linkImagesToOriginal: false
            }
          },
          {
            resolve: "gatsby-remark-emojis",
            options: {
              // Deactivate the plugin globally (default: true)
              active: true,
              // Add a custom css class
              class: "emoji-icon",
              // Select the size (available size: 16, 24, 32, 64)
              size: 64,
              // Add custom styles
              styles: {
                display: "inline",
                margin: "0",
                "margin-top": "1px",
                position: "relative",
                top: "5px",
                width: "25px"
              }
            }
          }
        ]
      }
    },
    {
      resolve: `gatsby-theme-cooper-hewitt`,
      options: {}
    },
    "gatsby-plugin-theme-ui"
  ]
};
