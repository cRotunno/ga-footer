/** @jsx jsx */
import React from "react";
import { Styled, jsx } from "theme-ui";
import { Link } from "gatsby";
import "./bio-content.scss";
/**
 * Shadow me to add your own bio content
 */

export default () => (
  <div>
    <div sx={{ mb: 2 }}>
      Greetings from{" "}
      <Styled.a id="link" href="http://rotunno.io/">Chris Rotunno</Styled.a>.
      <br />
      "Whoever controls the media, the images, contols the culture" -AG
    </div>
    <div sx={{ mb: 0 }}>
      <Styled.a id="link" as={Link} to="/about">
        More about me
      </Styled.a>
    </div>
  </div>
);
